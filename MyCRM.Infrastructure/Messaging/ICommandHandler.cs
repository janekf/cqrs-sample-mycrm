﻿namespace MyCRM.Infrastructure.Messaging
{
    public interface ICommandHandler<T> : ICommandHandler 
        where T : DomainCommand 
    {
        void Execute(T erstellenCommand);
    }

    public interface ICommandHandler{}
}