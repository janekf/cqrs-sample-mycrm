﻿namespace MyCRM.Infrastructure.Messaging
{
    public interface IEventHandler<T> where T : DomainEvent
    {
        void Handle(T domainEvent);
    }
}