﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;

namespace MyCRM.Infrastructure.Messaging
{
    public interface IMessageBus
    {
        void Send(IMessage message);
    	void Merge(IMessageBus other);
        IDisposable Register<T>(Action<T> action) where T : IMessage;
		IDisposable RegisterWithScheduler<T>(Action<T> action, IScheduler scheduler) where T : IMessage;
		IDisposable RegisterWithThrotteledExecution<T>(Action<T> action, int throttleTimeInMilliseconds) where T : IMessage;
		IDisposable RegisterWithThrotteledAndDistinctExecution<T>(Action<T> action, int throttleTimeInMilliseconds, IEqualityComparer<T> comparer) where T : IMessage;

    	void RejectMessageWithException(IMessage message, Exception exeption);

    	IObservable<IMessage> Subject { get; }
    }
}