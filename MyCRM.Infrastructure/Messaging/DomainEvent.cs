﻿using System;

namespace MyCRM.Infrastructure.Messaging
{
    public abstract class DomainEvent : IMessage
    {
        public Guid AggregateId { get; set; }
    }
}