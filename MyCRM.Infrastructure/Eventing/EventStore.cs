using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using MyCRM.Infrastructure.Messaging;
using MyCRM.Infrastructure.Repositories;
using MyCRM.Infrastructure.Storage;

namespace MyCRM.Infrastructure.Eventing
{
	public class EventStore : IEventStore
	{
		private readonly IEventStoreUnitOfWork _unitOfWork;
		private readonly IMessageBus _eventPublisher;

		public class EventDescriptor : IEntity
		{
			public DomainEvent DomainEventData { get; set; }
			public Guid AggregateId { get; set; }

			public Guid Id { get; set; }
			public int Version { get; set; }
			public DateTime TimeStamp { get; set; }
		}

		public EventStore(IMessageBus eventPublisher, IEventStoreUnitOfWork unitOfWork)
		{
			_eventPublisher = eventPublisher;
			_unitOfWork = unitOfWork;
		}

		public void SaveEvents(Guid aggregateId, IEnumerable<DomainEvent> events)
		{

			events.Do(e => { if (e.AggregateId == Guid.Empty) throw new ArgumentException(); });

			var eventsToSave = events.Select(x => new EventDescriptor()
													  {
														  DomainEventData = x,
														  AggregateId = aggregateId,
														  Id = Guid.NewGuid(),
														  TimeStamp = DateTime.UtcNow
													  }).ToList();
			try
			{
				using (var transaction = new TransactionScope())
				{
					foreach (var eventDescriptor in eventsToSave)
					{
						_unitOfWork.Store(eventDescriptor);
					}

					_unitOfWork.SaveChanges();

					foreach (var eventDescriptor in eventsToSave)
					{
						_eventPublisher.Send(eventDescriptor.DomainEventData);
					}
				}
			}
			catch(TransactionAbortedException tex)
			{
				Trace.WriteLine("Transaction aborted cause: " + tex.Message);
			}
			catch(ApplicationException aex)
			{
				Trace.WriteLine("Application exception was thrown: " + aex.Message);
			}
		}

		public IEnumerable<DomainEvent> GetEventsForEntity(Guid aggregateId)
		{
			return (from eventDescriptor in _unitOfWork.Query<EventDescriptor>()
					where eventDescriptor.AggregateId == aggregateId
					orderby eventDescriptor.Version
					select eventDescriptor).ToList().Select(x => x.DomainEventData);
		}
	}
}