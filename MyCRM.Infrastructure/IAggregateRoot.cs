﻿using System;
using System.Collections.Generic;
using MyCRM.Infrastructure.Messaging;

namespace MyCRM.Infrastructure
{
    public interface IAggregateRoot
    {
        Guid AggregateId { get; }

        void RestoreFromEventHistory(IEnumerable<DomainEvent> domainEvents);
        void MarkChangesAsCommitted();
        IEnumerable<DomainEvent> GetUncommittedChanges();
    }
}