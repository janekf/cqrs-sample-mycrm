﻿using System;
using System.Collections.Generic;
using MyCRM.Infrastructure.Messaging;

namespace MyCRM.Infrastructure.Storage
{
    public interface IEventStore
    {
        void SaveEvents(Guid entityId, IEnumerable<DomainEvent> events);        
        IEnumerable<DomainEvent> GetEventsForEntity(Guid aggregateId);
    }
}