﻿using System;

namespace MyCRM.Infrastructure.Storage
{
    public class Entity : IEntity
    {
        #region IEntity Members

        public Guid Id { get; set; }

        #endregion
    }

    public abstract class AbstractEntity : IEntity
    {
        #region IEntity Members

        public abstract Guid Id { get; set; }
        public int Version { get; set; }

        #endregion
    }
}