﻿using System;
using System.Data.SqlClient;
using System.Linq;

namespace MyCRM.Infrastructure.Storage
{
    public class MsSqlServerUnitOfWork : IUnitOfWork
    {
        SqlConnection _conn;
 
        public MsSqlServerUnitOfWork(string connectionString)
        {
            _conn = new SqlConnection(connectionString);
        }

        public void Dispose()
        {
            _conn.Dispose();
        }

        public IQueryable<T> Query<T>() where T : class, new()
        {
            throw new NotImplementedException();
        }

        public void Delete<T>(T entity) where T : class, IEntity, new()
        {
            throw new NotImplementedException();
        }

        public void Delete(object entity)
        {
            throw new NotImplementedException();
        }

        public void Store<T>(T entity) where T : class, IEntity, new()
        {
            throw new NotImplementedException();
        }

        public void Store(object entity)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }
    }
}
