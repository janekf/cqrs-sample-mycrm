﻿using System;
using MyCRM.Infrastructure.Storage;

namespace MyCRM.ReadModel.ViewModels
{
    public class CustomerViewModel : Entity
    {
        public Guid CustomerId { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string Street { get; set; }

        public string Number { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }
    }
}