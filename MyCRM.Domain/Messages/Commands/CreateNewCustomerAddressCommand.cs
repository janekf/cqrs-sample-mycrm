﻿using System;
using MyCRM.Infrastructure.Messaging;

namespace MyCRM.Domain.Messages.Commands
{
    public class CreateNewCustomerAddressCommand : DomainCommand
    {
        public Guid CuatomerId { get; set; }

        public string Street { get; set; }

        public string Number { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }
    }
}