﻿using System;
using MyCRM.Infrastructure;
using MyCRM.Infrastructure.Messaging;
using MyCRM.Infrastructure.Repositories;

namespace MyCRM.Domain
{
	internal static class CommandHandlerExtensions
	{
		public static IEventSourceRepository EventRepository
		{
			get { return ServiceLocator.GetInstance<IEventSourceRepository>(); ; }
		}

		public static TAggregate GetAggregateBy<TAggregate>(this ICommandHandler source, Guid aggregateId)
			where TAggregate : AggregateRoot, new()
		{
			return EventRepository.GetEventsBy<TAggregate>(aggregateId);
		}

		public static void SaveAggregate<TAggregate>(this ICommandHandler source, TAggregate aggregate)
			where TAggregate : AggregateRoot
		{
			EventRepository.SaveEvents(aggregate);
		}

		public static IReadModelRepository QueryRepository(this ICommandHandler source)
		{
			return ServiceLocator.GetInstance<IReadModelRepository>();
		}
	}
}
