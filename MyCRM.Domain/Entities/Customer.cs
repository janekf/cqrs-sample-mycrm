﻿namespace MyCRM.Domain.Entities
{
    internal class Customer
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}