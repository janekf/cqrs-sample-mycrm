﻿using System;

namespace MyCRM.WebApplication.Models
{
    public class ChangeCustomerAddressViewModel
    {
        public Guid CustomerId { get; set; }

        public int  Version { get; set; }

        public string LastName { get; set; }

        public string FirstName { get; set; }

        public string Street { get; set; }

        public string Number { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }


    }
}