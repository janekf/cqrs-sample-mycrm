﻿using System;
using MyCRM.Infrastructure;
using MyCRM.Infrastructure.Eventing;
using MyCRM.Infrastructure.Repositories;
using MyCRM.Infrastructure.Storage;
using StructureMap.Configuration.DSL;

namespace MyCRM.WebApplication.Bootstrapping
{
    internal class StoringRegistry : Registry
    {
        public StoringRegistry()
        {
            SetupRegistry();
        }

        private void SetupRegistry()
        {
            For<IEventStoreUnitOfWork>().UseSpecial(x =>
                x.ConstructedBy(() => new RavenDbUnitOfWork(
                    new Uri(Configuration.EventStoreConnectionString), Configuration.EventStoreDbName)));

            For<IReadModelUnitOfWork>().UseSpecial(x =>
                x.ConstructedBy(() => new RavenDbUnitOfWork(
                    new Uri(Configuration.ReadModelConnectionString), Configuration.ReadModelDbName)));

            For<IEventStore>().Use<EventStore>();
            For<IEventSourceRepository>().Use<EventSourceRepository>();
            For<IReadModelRepository>().Use<ReadModelRepository>();
        }
    }
}