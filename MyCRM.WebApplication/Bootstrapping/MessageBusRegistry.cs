﻿using MyCRM.Infrastructure.Messaging;
using StructureMap.Configuration.DSL;

namespace MyCRM.WebApplication.Bootstrapping
{
    internal class MessageBusRegistry : Registry
    {
        public MessageBusRegistry()
        {
            SetupRegistry();
        }

        private void SetupRegistry()
        {
            For<IMessageBus>().Singleton().Use<RxMessageBus>();
        }
    }
}
